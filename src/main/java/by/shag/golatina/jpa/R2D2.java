package by.shag.golatina.jpa;

public class R2D2 implements Robot{

    private String name = "R2D2";

    @Override
    public void sayName(){
        System.out.println("I am " + name);
    }
}
