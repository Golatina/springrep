package by.shag.golatina.jpa;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class T1000 implements Robot{

    @Value("${t1000.name}")
    //value - пойдет в ресурсах искать, есть ли такое св-во где-то в ресурсах, если есть, просетает. "${t1000.name:}" - с двоеточием, если не найдет, то ставит null
    private String name = "T-1000";

    @Override
    public void sayName(){
        System.out.println("I am " + name);
    }
}
