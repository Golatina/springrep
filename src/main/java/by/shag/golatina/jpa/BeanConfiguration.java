package by.shag.golatina.jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
//configuration - класс содержит инструкцию по созданию бинов
public class BeanConfiguration {

    @Bean
    public RobotShop robotShop1(){
        RobotShop robotShop = new RobotShop();
        robotShop.setName("magaz");
       return robotShop;
    }

    @Primary
    @Bean
    public RobotShop robotShop2(){
        RobotShop robotShop = new RobotShop();
        robotShop.setName("mmmmag");
        return robotShop;
    }

}
