package by.shag.golatina.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Component
public class RobotShop {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    //    @Autowired
//    private T1000 t1000;
//    @Autowired
//    private Terminator terminator;

//    @Qualifier("t1000")
//    @Autowired
    //Autowired - поискать в context объект , вставить. это как раз про впрыскивание, dependency injection
    //Context - ios - Inversion of Control - передача управления, доп к контексту Controller, Repository, Service (у них контекст есть аннотацией) - spring создает сам объекты, которые может
//    private Robot t1000;
//    @Qualifier("arni")
    //Qualifier - поиск по имени
//    @Autowired
//    private Robot terminator2;

    @Qualifier("t1000")
    @Autowired
    private Robot t1000;
    @Qualifier("archi")
    @Autowired(required = false)
    private Robot terminator2;

    public void show(String name) {
        if ("T1000".equals(name)) {
            t1000.sayName();
        } else {
            terminator2.sayName();
        }
    }

}
