package by.shag.golatina;

import by.shag.golatina.jpa.RobotShop;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.PropertySource;


@PropertySource("db.properties")
//propertySource - по умолчанию видит только файлы application \ bootstart . если нам надо из другого файла подтянуть (ибо апп общая настройка, а ни разу не для прописывания полей для классов)
@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
        System.out.println("________________________________________");
//        Object obj = context.getBean("runner");
//        T1000 robot = context.getBean(T1000.class);
//        Terminator terminator = context.getBean(Terminator.class);

//        R2D2 r2D2 = context.getBean(R2D2.class);
//        r2D2.sayName();
//        System.out.println("_________________________________________");

//        robot.sayName();
//        System.out.println("_________________________________________");
//        terminator.sayName();
//        System.out.println("_________________________________________");

        RobotShop robotShop = context.getBean(RobotShop.class);
        robotShop.show("T1000");
        robotShop.show("Terminator");

        RobotShop robotShop1 = (RobotShop) context.getBean("robotShop1");
        robotShop1.show("T1000");
        robotShop1.show("Terminator");

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

}
